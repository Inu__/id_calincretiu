import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blueGrey[900],
        body: SafeArea(
          child: PageView(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 20,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      radius: 75,
                      backgroundImage: AssetImage('assets/eu.jpg'),
                    ),
                    SizedBox(height: 50),
                    FittedBox(
                      child: Text(
                        'Călin Bogdan Crețiu',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Montserrat',
                          fontStyle: FontStyle.normal,
                          fontSize: 40,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    FittedBox(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          // Text(
                          //   'Online Marketing, 360 Photo & Video ',
                          //   style: TextStyle(
                          //     color: Colors.white60,
                          //     fontFamily: 'SourceSansPro',
                          //     fontStyle: FontStyle.normal,
                          //     fontSize: 25,
                          //     fontWeight: FontWeight.w100,
                          //   ),
                          // ),
                          // SizedBox(
                          //   height: 10,
                          // ),
                          Text(
                            '360° Photography, Advertising',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'SourceSansPro',
                              fontStyle: FontStyle.normal,
                              fontSize: 20,
                              fontWeight: FontWeight.w100,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Card(
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 0,
                          ),
                          leading: Icon(
                            Icons.phone,
                            size: 30,
                            color: Colors.teal[900],
                          ),
                          title: Text(
                            '+40 773 325 312',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Card(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 0,
                          ),
                          leading: Icon(
                            Icons.email,
                            size: 30,
                            color: Colors.teal[900],
                          ),
                          title: Text(
                            'calin.cretiu@gmail.com',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 60,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
